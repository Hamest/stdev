//
//  Helper.swift
//  STDevTask
//
//  Created by User on 4/19/18.
//  Copyright © 2018 Hamest. All rights reserved.
//

import UIKit
import CoreData

class Helper: NSObject {
    
    static let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    static let urlFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Url")
    
    
    static func fetchMaxID() ->(Int16) {
        let sortDescriptor = NSSortDescriptor(key: "urlID", ascending: false)
        urlFetch.sortDescriptors = [sortDescriptor]
        do {
            let urls = try! context.fetch(urlFetch) as! [Url]
            if urls.count > 0 {
                let max = urls.first
                return max?.value(forKey: "urlID") as! Int16
            } else {
                return 1
            }
        } catch _ {
            return 0
        }
    }
    
    static func getUrlWithID(id:Int16) ->(Url?) {
        urlFetch.predicate = NSPredicate(format: "urlID == %@", String(id))
        do {
            let urls = try! context.fetch(urlFetch) as! [Url]
            let currentUrl = urls.first
            urlFetch.predicate = nil
            return currentUrl
        } catch {
            urlFetch.predicate = nil
            print("can't get object")
            return nil
        }
    }
    
    static func updateObject(url:URLObject) {
        print(url.urlID)
        let currentUrl = self.getUrlWithID(id:url.urlID)
        print(currentUrl!)
        if currentUrl != nil {
            currentUrl?.responseInterval = url.responseInterval!
            currentUrl?.isReachable = url.isReachable!
            currentUrl?.isLoading = url.isLoading!
            print(currentUrl!)
            do {
                try context.save()
            } catch {
                print("Failed saving")
            }
        }
    }

    static func delate(id:Int16) {
        urlFetch.predicate = NSPredicate(format: "urlID == %@", String(id))
        do {
            let objects = try context.fetch(urlFetch)
            for object in objects {
                context.delete(object as! NSManagedObject)
            }
            try context.save()
        } catch _ {
            // error handling
        }
    }
    
    static func addObject(url:URLObject) -> NSManagedObject? {
        let entity = NSEntityDescription.entity(forEntityName: "Url", in: context)
        let newUrl = NSManagedObject(entity: entity!, insertInto: context)
        newUrl.setValue(url.urlID, forKey: "urlID")
        newUrl.setValue(url.address, forKey: "address")
        newUrl.setValue(url.responseInterval, forKey: "responseInterval")
        newUrl.setValue(url.isReachable, forKey: "isReachable")
        newUrl.setValue(url.isLoading, forKey: "isLoading")
        do {
            try context.save()
            return newUrl
        } catch {
            print("Failed saving")
            return nil
        }
    }
}
