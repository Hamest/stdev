//
//  URLObject.swift
//  STDevTask
//
//  Created by User on 4/18/18.
//  Copyright © 2018 Hamest. All rights reserved.
//

import UIKit

class URLObject: NSObject {
    var address: String?
    var isReachable: Bool?
    var isLoading: Bool?
    var responseInterval:Double?
    var urlID: Int16
    init(address: String,isReachable: Bool, responseInterval:Double,id:Int16,isLoading:Bool) {
        self.address = address
        self.isReachable = isReachable
        self.isLoading = isLoading
        self.responseInterval = responseInterval
        self.urlID = id
    }
}
