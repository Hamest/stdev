//
//  ViewController.swift
//  STDevTask
//
//  Created by User on 4/18/18.
//  Copyright © 2018 Hamest. All rights reserved.
//

import UIKit
import CoreData
enum OrderTypes {
    case nameAZ, nameZA, reachable, notReachable, maxTimeInterval, minTimeInterval
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortByTimeButton: UIButton!
    @IBOutlet weak var sortByReachabilityButton: UIButton!
    @IBOutlet weak var sortByNameButton: UIButton!
    @IBOutlet weak var urlTextField: UITextField!
    @IBOutlet weak var addUrlDialogView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let urlFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Url")
    var currentOrderType = OrderTypes.nameAZ
    var urlList : [Url] = []
    var filteredData: [Url]!
    var listToDelate: [Url] = []
    var maxID = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        orderByType(orderType:currentOrderType)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
// MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! CustomTableViewCell
        let url = filteredData[indexPath.row]
        if(self.listToDelate.contains(url)){
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        }else{
            cell.accessoryType = UITableViewCellAccessoryType.none
        }
        cell.addressLabel?.text = url.address
        cell.responseTimeLabel?.text =  String(format:"%f", url.responseInterval)
        if url.isLoading {
            cell.activityIndicator.startAnimating()
        } else {
            cell.activityIndicator.stopAnimating()
            if url.isReachable {
                cell.statusView.backgroundColor = UIColor.green
            } else {
                cell.statusView.backgroundColor = UIColor.red
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath as IndexPath)
        let url = filteredData[indexPath.row]
        if(self.listToDelate.contains(url)) {
            cell?.accessoryType = UITableViewCellAccessoryType.none
            if let checkedItemIndex = self.listToDelate.index(of: url){
                self.listToDelate.remove(at: checkedItemIndex)
            }
        } else {
            cell?.accessoryType = UITableViewCellAccessoryType.checkmark
            self.listToDelate.append(url)
        }
    }
    
    // MARK: - UISearchBarDelegate

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredData = searchText.isEmpty ? urlList : urlList.filter { (url: Url) -> Bool in
            return (url.address?.lowercased().contains(searchText.lowercased()))!
        }
        tableView.reloadData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.orderByType(orderType:self.currentOrderType)
        self.tableView.reloadData()
    }

// MARK: Button Actions

    @IBAction func refreshButtonAction(_ sender: Any) {
        self.searchBar.text = ""

        for url in self.urlList {
            let urlObject = URLObject(address: url.address!, isReachable: url.isReachable, responseInterval:url.responseInterval, id:url.urlID,isLoading:url.isLoading)
            update(url: urlObject)
        }
    }
    
    @IBAction func delateButtonAction(_ sender: Any) {
        self.searchBar.text = ""
        for url in self.listToDelate {
            if url.isLoading {
                let alert = UIAlertController(title: "Error", message: "Քայլ արա՛, մերժի՛ր սերժին ... ))", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                Helper.delate(id: url.urlID)
                if let index = self.urlList.index(of:url) {
                    self.urlList.remove(at: index)
                }
            }
        }
        self.filteredData = self.urlList
        print(self.urlList.count)
        print(self.filteredData.count)
        self.listToDelate = []
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    @IBAction func addURLButtonAction(_ sender: Any) {
        var array = ["https://zapier.com/", "https://address.gov.sa/en/", "https://medium.com", "https://stackoverflow.com","https://www.apple.com/","https://en.wikipedia.org/wiki/Steve_Jobs"]
        let randomIndex = Int(arc4random_uniform(UInt32(array.count)))
        self.urlTextField.text = array[randomIndex]
        self.addUrlDialogView.isHidden = false;
    }
    
    @IBAction func sortByNameAZButtonAction(_ sender: Any) {
        orderByType(orderType: OrderTypes.nameAZ)
    }
    @IBAction func sortByNameZAButtonAction(_ sender: Any) {
        orderByType(orderType: OrderTypes.nameZA)
    }
    
    @IBAction func sortByReachabileButtonAction(_ sender: Any) {
        orderByType(orderType: OrderTypes.reachable)
    }
    
    @IBAction func sortByNorReachabileButtonAction(_ sender: Any) {
        orderByType(orderType: OrderTypes.notReachable)
    }
    
    @IBAction func sortByMaxTimeButtonAction(_ sender: Any) {
        orderByType(orderType: OrderTypes.maxTimeInterval)
    }
    
    @IBAction func sortByMinTimeButtonAction(_ sender: Any) {
        orderByType(orderType: OrderTypes.minTimeInterval)
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        if self.urlTextField.isFirstResponder {
            self.urlTextField.resignFirstResponder()
        }
        self.addUrlDialogView.isHidden = true;
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        if self.urlTextField.isFirstResponder {
            self.urlTextField.resignFirstResponder()
        }
        self.addUrlDialogView.isHidden = true;
        let fatchID = Int(Helper.fetchMaxID())
        if maxID == 0 && fatchID == 0 {
            print("Failed saving")
        } else {
            maxID = fatchID
        }
        let url = URLObject(address: self.urlTextField.text!, isReachable: false, responseInterval:0, id:Int16(maxID+1),isLoading:true)
        
        self.addObject(url: url)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        update(url: url)
        self.urlTextField.text = ""
    }
    
    // MARK: Main Functions
    
    func orderByType(orderType:OrderTypes) {
        self.searchBar .resignFirstResponder()
        self.searchBar.text = ""
        self.currentOrderType = orderType
        var sortDescriptors: [NSSortDescriptor]?
        switch orderType {
        case .nameAZ:
            sortDescriptors = [NSSortDescriptor.init(key: "address", ascending: true)]
        case .nameZA:
            sortDescriptors = [NSSortDescriptor.init(key: "address", ascending: false)]
        case .reachable:
            sortDescriptors = [NSSortDescriptor.init(key: "isReachable", ascending: false)]
        case .notReachable:
            sortDescriptors = [NSSortDescriptor.init(key: "isReachable", ascending: true)]
        case .maxTimeInterval:
            sortDescriptors = [NSSortDescriptor.init(key: "responseInterval", ascending: false)]
        case .minTimeInterval:
            sortDescriptors = [NSSortDescriptor.init(key: "responseInterval", ascending: true)]
        }
        getURLs(sortDescriptors: sortDescriptors)
        self.tableView.reloadData()
    }
    
    func checkURLAddress(urlObject:URLObject,completion: @escaping (URLObject) -> Void ) {
        guard let urlAddress = URL(string: urlObject.address!) else { return }
        
        var request = URLRequest(url: urlAddress)
        request.timeoutInterval = 30.0
        let start:TimeInterval = NSDate.timeIntervalSinceReferenceDate

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let timeResponse:TimeInterval = NSDate.timeIntervalSinceReferenceDate - start
            print(timeResponse)
            urlObject.responseInterval = timeResponse
            urlObject.isLoading = false
            if let error = error {
                print("\(error.localizedDescription)")
                urlObject.isReachable = false
            }
            if (response as? HTTPURLResponse) != nil {
                urlObject.isReachable = true
            }
            completion(urlObject)
        }
        task.resume()
    }

    func getURLs(sortDescriptors:[NSSortDescriptor]?) {
        self.urlFetch.sortDescriptors = sortDescriptors
        self.urlList = try! self.context.fetch(self.urlFetch) as! [Url]
        self.filteredData = self.urlList
    }
    
    func addObject(url:URLObject) {
        let result = Helper.addObject(url: url)
        if (result != nil) {
            self.urlList .append(result as! Url)
            self.filteredData = self.urlList
        }
    }
    
    func update(url:URLObject) {
        
        checkURLAddress(urlObject: url) { (url) in
            Helper.updateObject(url: url)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
}
