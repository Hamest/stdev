//
//  CustomTableViewCell.swift
//  STDevTask
//
//  Created by User on 4/19/18.
//  Copyright © 2018 Hamest. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var responseTimeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func startLoading() {
        self.activityIndicator.startAnimating()
    }
    func stopLoading() {
        self.activityIndicator.stopAnimating()
    }

}
